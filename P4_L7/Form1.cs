﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P4_L7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(this.textBox1.TextLength < 1)
            {
                this.errorProvider1.SetError(this.textBox1, "Wypełnij pole");
            }
            else MessageBox.Show(this.textBox1.Text);
        }
    }
}
